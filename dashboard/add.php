<?php
include "inc/connection.php";
include "inc/function.php";

if(isset($_GET['yono'])) {
   inputData($_GET);
   header('location: indexs.php');
}


?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <title>Data Extra</title>
<style>
* {
  box-sizing: border-box;
}

/* Create a column layout with Flexbox */
.row {
  display: flex;
}

/* Left column (menu) */
.left {
  flex: 35px;
  padding: 15px 0;
}

.left h2 {
  padding-left: 8px;
}

/* Right column (page content) */
.right {
  flex: 65%;
  padding: 15px;
}

/* Style the search box */
#mySearch {
  width: 100%;
  font-size: 18px;
  padding: 11px;
  border: 1px solid #ddd;
}

/* Style the navigation menu inside the left column */
#myMenu {
  list-style-type: none;
  padding: 0;
  margin: 0;
}

#myMenu li a {
  padding: 12px;
  text-decoration: none;
  color: black;
  display: block
}

#myMenu li a:hover {
  background-color: #eee;
}

.overflow{
  width: 1100px;
  height:600px;
  overflow-y: scroll;
}
</style>
  </head>
  <body>

  <nav class="navbar navbar-light bg-dark">
      <a class="navbar-brand">
        <strong style="color:white;">Data Extra </strong>
      </a>
      <div class="float-right">
      <span style="color:white"><i class="fad fa-calendar-star mr-2"></i></i><?php echo date('d F Y'); ?></span>
      </div>
  </nav>
<div class="container-fluid">
  <div class="row">
  <div class="left" style="background-color:#bbb;">
      <strong><h2 class="mr-3"><i class="fas fa-bars"></i> Menu</h2></strong>
      <hr>
      <ul id="myMenu" style="margin-bottom: 300px;">
        <strong><li><a href="index.php"><i class="fal fa-globe mr-3"></i> Dashboard</a></li></strong>
        <li><a href="indexs.php"> <i class="fal fa-user mr-3"></i> Data Siswa</a></li>
        <li><a href="add.php"> <i class="fal fa-user-plus mr-2"></i>  Add Data</a></li>
        <li><a href="about.php"> <i class="fal fa-info-circle mr-2" style="font-size: 20px;"></i>  About</a></li>
      </ul>
    </div>
    
    <div class="right" style="background-color:#ddd;">
      <div class="container">
        <form action="indexs.php" method="GET">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" name="nama"  class="form-control" autocomplete="off" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Jurusan</label>
                <input type="text" name="jurusan"  class="form-control" autocomplete="off" required>
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Email address</label>
                <input type="email"  name="email" class="form-control" autocomplete="off" placeholder="Nama@gmail.com">
            </div>
            <div class="form-group w-50">
                <label for="exampleFormControlInput1">Gender</label>
                <select name="gender" class="custom-select">
                    <option selected>Open this select menu</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </div>
            <button type="submit" name="yono" class="btn btn-dark">Tambah</button>
        </form>
      </div>
    </div>
  </div>
</div>



  
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>