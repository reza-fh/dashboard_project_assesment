-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2020 at 08:07 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `extra`
--

-- --------------------------------------------------------

--
-- Table structure for table `robotic`
--

CREATE TABLE `robotic` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `jurusan` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `robotic`
--

INSERT INTO `robotic` (`id`, `nama`, `jurusan`, `email`, `gender`) VALUES
(84, 'Alex', 'Tkr', 'Alex@gmail.com', 'Female'),
(86, 'ncncnc', 'ncnc', 'ncn@gmail.com', 'Male'),
(87, 'Udin', 'Cbd', 'CbdUdin@gmail.com', 'Female'),
(91, 'Akbar', 'Perbangkan', 'akbar@gmail.com', 'Female'),
(93, 'aa', 'aa', 'aa@gmail.com', 'Male'),
(94, 'hrhr', 'hrhr', 'hhh@gmail111.com', 'Male'),
(96, 'namaa', 'naam', 'nama@gmail.com', 'Male');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `robotic`
--
ALTER TABLE `robotic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `robotic`
--
ALTER TABLE `robotic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
