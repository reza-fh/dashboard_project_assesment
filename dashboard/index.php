<?php
include "inc/connection.php";

$temp_arr = count($data);

$female = $db->query("SELECT * FROM robotic WHERE gender='female'");
$female_data = $female->fetchAll();
$temp_female = count($female_data);

$male = $db->query("SELECT * FROM robotic WHERE gender='male'");
$male_data = $male->fetchAll();
$temp_male = count($male_data);

?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js"></script>
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <title>Data Extra</title>
<style>
* {
  box-sizing: border-box;
}

/* Create a column layout with Flexbox */
.row {
  display: flex;
}

/* Left column (menu) */
.left {
  flex: 35px;
  padding: 15px 0;
}

.left h2 {
  padding-left: 8px;
}

/* Right column (page content) */
.right {
  flex: 65%;
  padding: 15px;
}

/* Style the search box */
#mySearch {
  width: 100%;
  font-size: 18px;
  padding: 11px;
  border: 1px solid #ddd;
}

/* Style the navigation menu inside the left column */
#myMenu {
  list-style-type: none;
  padding: 0;
  margin: 0;
}

#myMenu li a {
  padding: 12px;
  text-decoration: none;
  color: black;
  display: block
}

#myMenu li a:hover {
  background-color: #eee;
}

.overflow{
  width:1100px;
  height:600px;
  overflow-y: scroll;
}

</style>
  </head>
  <body>

  <nav class="navbar navbar-light bg-dark">
      <a class="navbar-brand">
        <strong style="color:white;">Data Extra </strong>
      </a>
      <div class="float-right">
      <span style="color:white"><i class="fad fa-calendar-star mr-2"></i></i><?php echo date('d F Y'); ?></span>
      </div>
  </nav>
<div class="container-fluid">
  <div class="row">
    <div class="left" style="background-color:#bbb;">
      <strong><h2 class="mr-3"><i class="fas fa-bars"></i> Menu</h2></strong>
      <hr>
      <ul id="myMenu" style="margin-bottom: 40px;">
        <strong><li><a href="index.php"><i class="fal fa-globe mr-3"></i> Dashboard</a></li></strong>
        <li><a href="indexs.php"> <i class="fal fa-user mr-3"></i> Data Siswa</a></li>
        <li><a href="add.php"> <i class="fal fa-user-plus mr-2"></i>  Add Data</a></li>
        <li><a href="about.php"> <i class="fal fa-info-circle mr-2" style="font-size: 20px;"></i>  About</a></li>
      </ul>
    </div>
    <div class="overflow"  style="background-color:#ddd;">
      <div class="right">
      <div class="col-5 float-left">
        <div class="card text-white bg-dark mb-3 mt-3 shadow" style="max-width: 30rem;">
          <div class="card-body">
            <h5><i class="fas fa-users" style="font-size: 150px; float:left;"></i></h5>
            <center><h1 style="font-size: 100px;"><?php  echo $temp_arr; ?></h1></center>
            <p class="card-text float-left"> <strong>Total data for all who participate in Extra Robotic</strong></p>
          </div>
        </div>
      </div>
      <div class="col-7 float-left">
        <center><canvas id="myChart"></canvas></center>
        <center><strong>Data Male/Female</strong></center>
      </div>
      </div>
    </div>
  </div>
</div>
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script  type="text/javascript">
    var female = <?php echo $temp_female; ?>
    </script>
<script  type="text/javascript">
    var male = <?php echo $temp_male; ?>
</script>
    <script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['Female','Male'],
        datasets: [{
            label: '# of Votes',
            data: [female,male],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1
        }]
    },
});
</script>
</body>
</html>