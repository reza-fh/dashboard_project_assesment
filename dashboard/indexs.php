<?php
    include "inc/connection.php";
    include "inc/function.php";

    
    if(isset($_GET['yono'])) {
        inputData($_GET);
        header('location: indexs.php');
    }
    
    if(isset($_GET['delete'])){
        deleteData($_GET);
        header('location: indexs.php');
    }
    
    if (isset($_POST['edit'])) {
        edit($_POST);
        header("Location: indexs.php");
    }
    
    $tampung=[];
    if(isset($_POST['search'])){
        $tampung=searching($_POST['search']);
        if(!empty($tampung)){
            $data=$tampung;
        }else{
        $msg=$_POST['search']." Tidak Ditemukan!!";
        }
    }

    
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <title>Data Extra</title>
<style>

* {
  box-sizing: border-box;
}

/* Create a column layout with Flexbox */
.row {
  display: flex;
}

/* Left column (menu) */
.left {
  flex: 35px;
  padding: 15px 0;
}

.left h2 {
  padding-left: 8px;
}

/* Right column (page content) */
.right {
  flex: 65%;
  padding: 15px;
}

/* Style the search box */
#mySearch {
  width: 100%;
  font-size: 18px;
  padding: 11px;
  border: 1px solid #ddd;
}

/* Style the navigation menu inside the left column */
#myMenu {
  list-style-type: none;
  padding: 0;
  margin: 0;
}

#myMenu li a {
  padding: 12px;
  text-decoration: none;
  color: black;
  display: block
}

#myMenu li a:hover {
  background-color: #eee;
}

.overflow{
  width: 1100px;
  height:600px;
  overflow-y: scroll;
}
</style>
  </head>
  <body>

  

  <nav class="navbar navbar-light bg-dark">
      <a class="navbar-brand">
        <strong style="color:white;">Data Extra </strong>
      </a>
      <div class="float-right">
      <span style="color:white"><i class="fad fa-calendar-star mr-2"></i></i><?php echo date('d F Y'); ?></span>
      </div>
  </nav>
<div class="container-fluid">
  <div class="row">
  <div class="left" style="background-color:#bbb;">
      <strong><h2 class="mr-3"><i class="fas fa-bars"></i> Menu</h2></strong>
      <hr>
      <ul id="myMenu" style="margin-bottom: 30px;">
        <strong><li><a href="index.php"><i class="fal fa-globe mr-3"></i> Dashboard</a></li></strong>
        <li><a href="indexs.php"> <i class="fal fa-user mr-3"></i> Data Siswa</a></li>
        <li><a href="add.php"> <i class="fal fa-user-plus mr-2"></i>  Add Data</a></li>
        <li><a href="about.php"> <i class="fal fa-info-circle mr-2" style="font-size: 20px;"></i>  About</a></li>
      </ul>
    </div>
  
  <div class="overflow">
    <div class="right" style="background-color:#ddd;">
      <div class="container">
        <h4 class="float-left font-weight-bold">Data Siswa Robotic</h4>
        <form action="indexs.php" class="form-inline float-right mb-2" method="POST">
          <div class="md-form my-0">
              <input name="search" class="form-control mr-sm-2" type="search" placeholder="Cari data" aria-label="Search" autocomplete="off">
              <?php if(isset($msg)) : ?>
            <div class="alert alert-primary" role="alert">
              <?php echo $msg; ?>
            </div>
            <?php endif; ?>
          </div>
        </form>
      <table class="table border border-dark">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Card</th>
            <th scope="col">Nama</th>
            <th scope="col">Jurusan</th>
            <th scope="col">Email</th>
            <th scope="col">Gender</th>
            <th scope="col">Aksi</th>

        </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $key): ?>
            <tr>
              <td>
              <div class="card border border-dark" style="background-image:url(img/bg-card.jpg);">
              <div class="card-body border">
                <div class="float-left">
                  <i class="fad fa-user-alt" style="font-size: 70px;"></i>
                </div>  
                <div class="col-8 float-right">
                 <strong><i><h5 class="card-title"><?php echo $key["nama"]; ?></h5>
                  <h6 class="card-subtitle"><?php echo $key["jurusan"]; ?></h6>
                  <?php echo $key["email"]; ?></i></strong> 
                </div>
              </div>
            </div>
              </td>
              <td><strong><i><?php echo $key["nama"]; ?></i></strong></td>
              <td><strong><i><?php echo $key["jurusan"]; ?></i></strong></td>
              <td><strong><i><?php echo $key["email"]; ?></i></strong></td>
              <td><strong><i><?php echo $key["gender"]; ?></i></strong></td>
              <td>
                <a class="btn btn-outline-dark"  onclick="return confirm('Apakah Kamu Ingin Menghapus Data?')" href="indexs.php?delete=&id=<?php echo $key["id"]; ?>">hapus</a>
                <a class="btn btn-outline-dark"  data-toggle="modal" data-target="<?php echo "#".$key['nama']; ?>">edit</a>
              </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
</div>

<!-- POPUP EDIT -->

<!-- Modal -->
<?php foreach($data as $value): ?>
  <div class="modal fade" id="<?php echo $value['nama']; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="indexs.php" method="POST">
                <input type="hidden" name="id" value="<?php echo $value["id"]; ?>">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" name="nama" value="<?php echo $value["nama"]; ?>" class="form-control"  aria-describedby="emailHelp" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">jurusan</label>
                <input type="text" name="jurusan" value="<?php echo $value["jurusan"]; ?>" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">email</label>
                <input type="email" name="email" value="<?php echo $value["email"]; ?>" class="form-control" required>
            </div>
            <div class="form-group w-50">
                <select name="gender" class="custom-select">
                    <option value="Male" <?php if($value['gender']=="Male"){ echo " selected"; } ?>>Male</option>
                    <option value="Female" <?php if($value['gender']=="Female"){ echo " selected"; } ?>>Female</option>
                </select>
            </div>
            <button type="button" class="btn btn-secondary float-right  " data-dismiss="modal">Close</button>
            <button type="submit" name="edit" class="btn btn-primary float-right">Edit</button>
            </form>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>  

    <script>
        function myFunction() {
            var input, filter, ul, li, a, i;
            input = document.getElementById("mySearch");
            filter = input.value.toUpperCase();
            ul = document.getElementById("myMenu");
            li = ul.getElementsByTagName("li");
            for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
            }
        }
    </script>

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>